#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <math.h>
#include <malloc.h>
#include <SDL2/SDL.h>

#define AUDIO_BUFFER_LEN 4096
#define SCOPE_HEIGHT 512
#define SAMPLE_RATE 44100

// Chosen so that the frequency range of standard-tuning guitar strings yields a reasonable frame rate.
#define FRAME_SKIP 4

#define TITLE_LEN 32

typedef struct {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t x;
} pixel_t;

typedef struct {
    int16_t l;
    int16_t r;
} sample_t;

// Global variables
SDL_AudioDeviceID audioDevice = 0;
SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;
SDL_Texture *texture = NULL;
pixel_t *frameBuffer = NULL;

int periodSamples;
float sampleDuration;
int framePosition;
float positionFractional;

// Calculates the frequency of a given piano key.
float pianoFrequency(int key) {
    return 440.0f * powf(2.0f, (key - 49) / 12.0f);
}


// Apply the new framebuffer.
void updateFrame() {
    SDL_UpdateTexture(texture, NULL, frameBuffer, periodSamples * sizeof(pixel_t));
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderPresent(renderer);
}

// The audio data callback.
void audioCallback(void *userdata, Uint8 *stream, int len) {
    int i, j, x, y;
    pixel_t *pix;
    sample_t *samples = (sample_t*)stream;

    for (i = 0; i < len / sizeof(sample_t); i++) {
        if (framePosition > 0) {
            // The X coordinate.
            x = framePosition % periodSamples;

            // The Y coordinate.
            y = (int)(SCOPE_HEIGHT / 2.0f + samples[i].l / 32768.0f / 2.0f * SCOPE_HEIGHT);

            // Set the pixel color.
            pix = &frameBuffer[x + y * periodSamples];

            if (pix->g <= 255 - 51)
                pix->g += 51;
        }

        // Increment the fractional samples counter (and the frame position counter too).
        positionFractional += sampleDuration;
        while (positionFractional >= 1.0f) {
            positionFractional -= 1.0f;

            // Next sample.
            framePosition += 1;
        }

        // Next frame?
        if (framePosition >= periodSamples * FRAME_SKIP) {
            framePosition = 0;
            updateFrame();

            // Clear the frame buffer.
            //memset(frameBuffer, 0, periodSamples * SCOPE_HEIGHT * sizeof(pixel_t));

            // Decay
            for (j = 0; j < periodSamples * SCOPE_HEIGHT; j++) {
                if (frameBuffer[j].g >= 17)
                    frameBuffer[j].g -= 17;
            }
        }
    }
}

// Try initializing an audio capture device.
int initAudio() {
    SDL_AudioSpec want, have;
    SDL_zero(want);

    want.freq = SAMPLE_RATE;
    want.format = AUDIO_S16;
    want.channels = 2;
    want.samples = AUDIO_BUFFER_LEN;
    want.callback = audioCallback;

    // We want a capture device with the exact specs given above.
    if (!(audioDevice = SDL_OpenAudioDevice(NULL, 1, &want, &have, 0))) {
        fprintf(stderr, "Couldn't open an audio device: %s\n", SDL_GetError());
        return 0;
    }

    return 1;
}

// Clean up the SDL resources.
void finish() {
    if (texture)
        SDL_DestroyTexture(texture);

    if (renderer)
        SDL_DestroyRenderer(renderer);

    if (window)
        SDL_DestroyWindow(window);

    if (frameBuffer)
        free(frameBuffer);

    if (audioDevice)
        SDL_CloseAudioDevice(audioDevice);

    SDL_Quit();
}


int main(int argc, char *argv[]) {
    int tuneKey;
    float targetFrequency;
    float targetPeriod;
    SDL_Event event;
    char title[TITLE_LEN], *stringName;

    // Check the argument count.
    if (argc < 2 || strlen(argv[1]) != 1) {
        fprintf(stderr, "Usage: %s <guitar string: e,a,d,g,b,E>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Choose a piano key matching the target string.
    switch (argv[1][0]) {
        case 'e':
            tuneKey = 20;
            stringName = "E (low)";
            break;
        case 'a':
            tuneKey = 25;
            stringName = "A";
            break;
        case 'd':
            tuneKey = 30;
            stringName = "D";
            break;
        case 'g':
            tuneKey = 35;
            stringName = "G";
            break;
        case 'b':
            tuneKey = 39;
            stringName = "B";
            break;
        case 'E':
            tuneKey = 44;
            stringName = "E (high)";
            break;
        default:
            fprintf(stderr, "Invalid guitar string name specified.\n");
            return EXIT_FAILURE;
    }

    // Compute the target frequency and sample period.
    targetFrequency = pianoFrequency(tuneKey);
    targetPeriod = SAMPLE_RATE / targetFrequency * 3.0f;
    periodSamples = (int)targetPeriod;
    sampleDuration = periodSamples / targetPeriod;

    // Debug
    fprintf(stderr, "Target frequency: %f, period: %d (actual sample duration %f)\n", targetFrequency, periodSamples, sampleDuration);

    // Try initializing SDL.
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        fprintf(stderr, "SDL_Init failed: %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    // Try initializing audio.
    if (!initAudio()) {
        finish();
        return EXIT_FAILURE;
    }

    // Try creating a window and a renderer.
    if (SDL_CreateWindowAndRenderer(periodSamples, SCOPE_HEIGHT, 0, &window, &renderer) < 0) {
        fprintf(stderr, "SDL_CreateWindowAndRenderer failed: %s\n", SDL_GetError());
        finish();
        return EXIT_FAILURE;
    }

    // Set window title.
    snprintf(title, TITLE_LEN, "visual-tuner: %s", stringName);
    SDL_SetWindowTitle(window, title);

    // Try creating a texture for rendering.
    if (!(texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_STREAMING, periodSamples, SCOPE_HEIGHT))) {
        fprintf(stderr, "SDL_CreateTexture failed: %s\n", SDL_GetError());
        finish();
        return EXIT_FAILURE;
    }

    // Allocate memory for the frame.
    if (!(frameBuffer = calloc(periodSamples * SCOPE_HEIGHT, sizeof(pixel_t)))) {
        perror("Couldn't allocate the frame buffer");
        finish();
        return EXIT_FAILURE;
    }

    // Zero the frame sample counter and unpause the audio capture.
    framePosition = 0;
    positionFractional = 0;
    SDL_PauseAudioDevice(audioDevice, 0);

    // Wait for exit.
    while (1) {
        SDL_WaitEvent(&event);
        if (event.type == SDL_QUIT)
            break;
    }

    finish();
    return EXIT_SUCCESS;
}
