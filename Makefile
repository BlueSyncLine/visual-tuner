all: clean tuner

clean:
	rm -f *.o tuner

tuner: tuner.o
	gcc -o tuner -lm `sdl2-config --libs` tuner.o

tuner.o:
	gcc -Ofast -c -o tuner.o -Wall -Wextra `sdl2-config --cflags` main.c

